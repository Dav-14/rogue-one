const { Sequelize, DataTypes } = require("sequelize");

class databaseConnexion {
  #sequelizeInstance = null;
  static #dbInstance = null;

  constructor() {
    this.#sequelizeInstance = new Sequelize(
      process.env.BBDNAME,
      process.env.BBDUSERNAME,
      process.env.BBDPASSWORD,
      {
        host: process.env.BDDHOST,
        dialect: process.env.DIALECT,
        port: process.env.BBDPORT,
      }
    );
  }

  static get DbConnect() {
    if (!databaseConnexion.#dbInstance) {
      databaseConnexion.#dbInstance = new databaseConnexion();
    }
    return databaseConnexion.#dbInstance;
  }

  get sequelizeInstance() {
    return this.#sequelizeInstance;
  }

  testCo() {
    try {
      sequelize.authenticate();
      console.log("Connection has been established successfully.");
    } catch (error) {
      console.error("Unable to connect to the database:", error);
    }
  }
  model() {
    this.#sequelizeInstance.define(
      "User",
      {
        username: DataTypes.TEXT,
        password: DataTypes.TEXT,
        salt: DataTypes.TEXT,
      },
      { timestamps: false }
    );
  }
}
module.exports = databaseConnexion;
